# Webarchitects Debian Bookworm Ansible Role

This role is designed to assist in upgrading [Debian Bullseye](https://git.coop/webarch/bullseye) servers to Debian Bookworm, see also the [Bullseye role](https://git.coop/webarch/bullseye).

This role defaults to not removing non-Bookworm packages, not disabling files in `/etc/apt/sources.list.d`, changing _"bullseye"_ to _"bookworm"_ in files in `/etc/apt/sources.list.d`, this behaviour can be altered by changing the [defaults](defaults/main.yml).

This role includes the [upgrade role](https://git.coop/webarch/upgrade) to ensure that everything is upto date to start with.

This role includes the [apt role](https://git.coop/webarch/apt) to update the `/etc/apt/sources.list` and run `apt-get --allow-releaseinfo-change update`.

## Servers with a /chroot

SSH to the server and run the `bookworm.yml` playbook in check mode:

```bash
cd /root/chroot-config
git checkout master
git pull
./requirements.sh bookworm
export ANSIBLE_CONFIG=ansible_human.cfg
ansible-playbook -i hosts.yml -c chroot bookworm.yml -C
```

If that is OK, then run it not in check mode:

```bash
ansible-playbook -i hosts.yml -c chroot bookworm.yml --diff
```

And then chroot into the chroot and upgrade it:

```bash
chroot /chroot
apt remove php-composer-ca-bundle php-composer-class-map-generator \
    php-composer-metadata-minifier php-composer-pcre php-composer-semver \
    php-composer-spdx-licenses php-composer-xdebug-handler composer && \
apt upgrade && \
apt full-upgrade && \
apt install jc && \
apt autoremove
```

And then run the `chroot.yml` playbook to ensure everything is updated:

```bash
ansible-playbook -i hosts.yml -c chroot chroot.yml --diff
```

**NOTE** If `apache2` is chrooted then it'll fill the `/var/log/apache2/error.log` with errors like this:

```txt
libgcc_s.so.1 must be installed for pthread_cancel to work
```

Until the host is also updated.

## Upgrade servers

Run this role using a playbook like this, for example save the following as `bookworm.yml`:

```yaml
- name: Upgrade Debian Bullseye to Debian Bookworm
  become: true
  gather_facts: true
  hosts:
    - bullseye_servers
    - bookworm_servers
  vars:
    bookworm: true
  roles:
    - role: bookworm
      when:
        - bookworm is defined
        - bookworm | bool
    - role: upgrade
      when:
        - upgrade is defined
        - upgrade | bool
```

First run in check mode, for example:

```bash
ansible-playbook bookworm.yml -l host.example.org -C
```

Update the systemd config for the Ethernet device name when on Xen, it needs to be renamed from `eth0` to `enX0`.

Then run it to update the apt sources:

```bash
ansible-playbook bookworm.yml -l host.example.org
```

Then SSH to the server and check there is space to upgrade the server:

```bash
apt autoremove && apt clean && apt update && \
apt -o APT::Get::Trivial-Only=true full-upgrade
```

**Make sure there is at least 1GiB available for `/`.**

Remove `exim4-config` and sokme other packages as the upgrade failed with them installed:

```bash
apt purge exim4-config texlive-fonts-recommended texlive-latex-base texlive-latex-recommended
```

Upgrade the server:

```bash
apt upgrade --without-new-pkgs && \
apt full-upgrade
```

Reboot and `autoremove` and if the server is running Munin:

```bash
apt autoremove && \
rm -f /var/lib/munin-node/plugin-state/nobody/plugin-apt_all.state && \
cd /etc/munin/plugins && \
munin-run apt_all
```

Then run the role one last time:

```bash
ansible-playbook bookworm.yml -l host.example.org
```

Then run all the roles for the server.

## PostgreSQL

> ## Obsolete major version 13
>
> The PostgreSQL version 13 is obsolete, but the server or client packages are still installed. Please install the latest packages (postgresql-15 and postgresql-client-15) and upgrade the existing  clusters with pg_upgradecluster (see manpage).
>
> Please be aware that the installation of postgresql-15 will automatically create a default cluster 15/main. If you want to upgrade the 13/main cluster, you need to remove the already existing 15 cluster (pg_dropcluster --stop 15 main, see manpage for details).
>
> The old server and client packages are no longer supported. After the existing clusters are upgraded, the postgresql-13 and postgresql-client-13 packages should be removed.
>
> Please see /usr/share/doc/postgresql-common/README.Debian.gz for details.

Check if there are any clusters:

```bash
pg_lsclusters
```

After upgrading the OS PostgreSQL need to be upgraded:

```bash
pg_dropcluster --stop 15 main
pg_upgradecluster 13 main
apt remove postgresql-13 postgresql-client-13
```
